from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

import Tkinter as tk
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

#Animations set to finite time span. If running for infinite loop, unable to quit.
#If we wanted to run an infinte loop, we would just set the 'repeat' parameter in FuncAnimation to 'True'

TFont=("Times", 12)


class Interface(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.CreateWidgets()
        self.pack()
        self.font = ("Times", 18, "bold")

        self.G=9.81

        self.fig = plt.figure(dpi=60)
        self.fig2=plt.figure(dpi=60)

    def CreateWidgets(self):
        
        #single pendulum
        self.label_tinit = tk.Label(self, text ="Link 1").grid(column=1,row=0)

        self.angletext1 = tk.Label(self, text = "Angle", font = TFont).grid(column=1,row=1)
        self.angle1=tk.Scale(self, from_ = 0.00, to = 180.00, orient = tk.HORIZONTAL, resolution = 0.5)
        self.angle1.grid(column = 2, row = 1)
        self.angle1.set(90)

        self.lengthtext1 = tk.Label(self, text = "Length", font = TFont).grid(column=1,row=2)
        self.length1=tk.Scale(self, from_ = 0.00, to = 2.50, orient = tk.HORIZONTAL, resolution = 0.1)
        self.length1.grid(column = 2, row = 2)
        self.length1.set(1.0)

        self.masstext1 = tk.Label(self, text = "Mass", font = TFont).grid(column=1,row=3)
        self.mass1=tk.Scale(self, from_ = 0.00, to = 10, orient = tk.HORIZONTAL, resolution = 0.5)
        self.mass1.grid(column = 2, row = 3)
        self.mass1.set(1.0)

        self.frictiontext1 = tk.Label(self, text = "Friction", font = TFont).grid(column=1,row=4)
        self.friction1=tk.Scale(self, from_ = 0.00, to = 1.00, orient = tk.HORIZONTAL, resolution = 0.1)
        self.friction1.grid(column = 2, row = 4)
        self.friction1.set(0.00)

        self.angveltext1 = tk.Label(self, text = "Angular Velocity", font = TFont).grid(column=1,row=5)
        self.angvel1=tk.Scale(self, from_ = -50.00, to = 50.00, orient = tk.HORIZONTAL, resolution = 0.1)
        self.angvel1.grid(column = 2, row = 5)
        self.angvel1.set(0.00)


        #double pendulum (pendulum#2)
        self.label_tinit = tk.Label(self, text ="Link 2").grid(column=4,row=0)

        self.angletext2 = tk.Label(self, text = "Angle", font = TFont).grid(column=4,row=1)
        self.angle2=tk.Scale(self, from_ = 0.00, to = 180.00, orient = tk.HORIZONTAL, resolution = 0.5)
        self.angle2.grid(column = 5, row = 1)
        self.angle2.set(90)

        self.lengthtext2 = tk.Label(self, text = "Length", font = TFont).grid(column=4,row=2)
        self.length2=tk.Scale(self, from_ = 0.00, to = 2.50, orient = tk.HORIZONTAL, resolution = 0.1)
        self.length2.grid(column = 5, row = 2)
        self.length2.set(1.0)

        self.masstext2 = tk.Label(self, text = "Mass", font = TFont).grid(column=4,row=3)
        self.mass2=tk.Scale(self, from_ = 0.00, to = 10, orient = tk.HORIZONTAL, resolution = 0.5)
        self.mass2.grid(column = 5, row = 3)
        self.mass2.set(1.0)

        self.frictiontext2 = tk.Label(self, text = "Friction", font = TFont).grid(column=4,row=4)
        self.friction2=tk.Scale(self, from_ = 0.00, to = 0.00, orient = tk.HORIZONTAL, resolution = 0.1)
        self.friction2.grid(column = 5, row = 4)
        self.friction2.set(0.00)

        self.angveltext2 = tk.Label(self, text = "Angular Velocity", font = TFont).grid(column=4,row=5)
        self.angvel2=tk.Scale(self, from_ = -50.00, to = 50.00, orient = tk.HORIZONTAL, resolution = 0.1)
        self.angvel2.grid(column = 5, row = 5)
        self.angvel2.set(0.00)


        #general
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=7,row=5)
        self.single = tk.Button(self, text="Animate Single", command=self.single).grid(column=6,row=2)
        self.double = tk.Button(self, text="Animate Double", command=self.double).grid(column=7,row=2)

        self.plot = tk.Button(self, text=" Plot",command=self.plot).grid(column=6,row=3)

        self.label_tinit = tk.Label(self, text="Time = ", font=TFont).grid(column=1,row=7)
        self.label_tinit = tk.Label(self, text="Interval = ", font=TFont).grid(column=1,row=8)
                        
        self.time = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.time).grid(column=2,row=7)
        self.time.set("10.0")
        
        self.dt_entry = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.dt_entry).grid(column=2,row=8)
        self.dt_entry.set("0.05")

        #x, y, theta, angular speed drop down menu
        self.var=tk.StringVar(self.master)
        self.var.set("Choose Parameter to Plot")

        self.options=tk.OptionMenu(self,self.var,"Single X-Position","Single Y-Position","Single Angle","Single Angular Velocity",
            "Double X-Position","Double Y-Position", "Double Angle", "Double Angular Velocity").grid(column= 7, row=3)

        self.label_disclaimer = tk.Label(self, text ="*Friction only applies to single pendulum*").grid(column=7,row=0)


#used double pendulum code obtained from matplotlib 'http://matplotlib.org/examples/animation/double_pendulum_animated.html'
    def singlederivs(self, state, t):
        dydx = np.zeros_like(state)
        dydx[0] = state[1]
        dydx[1] = -(2*self.G/self.L1i)*np.sin(state[0]) - (self.F1i/self.M1i)*state[1]
        return dydx

    def  single_calc(self):
    	self.dt = float(self.dt_entry.get())
        self.t = np.arange(0.0, float(self.time.get()), self.dt)

        self.L1i=self.length1.get()
        self.M1i=self.mass1.get()
        self.F1i=self.friction1.get()

        # th1 and th2 are the initial angles (degrees)
        # w1 and w2 are the initial angular velocities (degrees per second)
        th1 = self.angle1.get()
        w1 = self.angvel1.get()


        rad = pi/180

        # initial state
        state = np.array([th1, w1])*pi/180.

        # integrate your ODE using scipy.integrate.
        y = integrate.odeint(self.singlederivs, state, self.t)

        self.sx1 = self.L1i*sin(y[:,0])
        self.sy1 = -self.L1i*cos(y[:,0])

        return self.sx1,self.sy1,y[:,0],y[:,1]


    def single(self):

        self.fig.clear()
        
        self.x1,self.y1,stheta,sthetadot=self.single_calc()


        canvas = FigureCanvasTkAgg(self.fig, master = self)
        canvas.get_tk_widget().grid(column=0,row=12,columnspan=10)
        
        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-5.0, 5.0), ylim=(-5.0, 5.0))
        ax.grid()


        self.line, = ax.plot([], [], 'o-', lw=2)
        self.line.set_data([], [])
        
        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        self.time_text.set_text('')


        self.ani = animation.FuncAnimation(self.fig, self.singleanimate, np.arange(1, len(stheta)),
            interval=25, blit=False, init_func=self.init,repeat=False)

        #ani.save('double_pendulum.mp4', fps=15)
        canvas.show()

    def init(self):
        return self.line, self.time_text

    def singleanimate(self, i):
        self.thissx = [0, self.sx1[i]]
        self.thissy = [0, self.sy1[i]]

        self.line.set_data(self.thissx, self.thissy)
        self.time_text.set_text(self.time_template%(i*self.dt))
        return self.line, self.time_text

    
    def doublederivs(self, state, t):

        dydx = np.zeros_like(state)
        dydx[0] = state[1]

        del_ = state[2]-state[0]
        den1 = (self.M1i+self.M2i)*self.L1i - self.M2i*self.L1i*cos(del_)*cos(del_)

        dydx[1] = (self.M2i*self.L1i*state[1]*state[1]*sin(del_)*cos(del_)
            + self.M2i*self.G*sin(state[2])*cos(del_) + self.M2i*self.L2i*state[3]*state[3]*sin(del_)
            - (self.M1i+self.M2i)*self.G*sin(state[0]))/den1

        dydx[2] = state[3]

        den2 = (self.L2i/self.L1i)*den1
        dydx[3] = (-self.M2i*self.L2i*state[3]*state[3]*sin(del_)*cos(del_)
            + (self.M1i+self.M2i)*self.G*sin(state[0])*cos(del_)
            - (self.M1i+self.M2i)*self.L1i*state[1]*state[1]*sin(del_)
            - (self.M1i+self.M2i)*self.G*sin(state[2]))/den2

        return dydx

    def double_calc(self):
    	

        self.dt = float(self.dt_entry.get())
        self.t = np.arange(0.0, float(self.time.get()), self.dt)

        # th1 and th2 are the initial angles (degrees)
        # w1 and w2 are the initial angular velocities (degrees per second)

        self.L1i=self.length1.get()
        self.M1i=self.mass1.get()
        self.F1i=self.friction1.get()

        self.L2i=self.length2.get()
        self.M2i=self.mass2.get()
        self.F2i=self.friction2.get()

        th1 = self.angle1.get()
        w1 = self.angvel1.get()
        th2 = self.angle2.get()
        w2 = self.angvel2.get()

        rad = pi/180

        # initial state
        state = np.array([th1, w1, th2, w2])*pi/180.

        # integrate your ODE using scipy.integrate.
        y = integrate.odeint(self.doublederivs, state, self.t)

        self.x1 = self.L1i*sin(y[:,0])
        self.y1 = -self.L1i*cos(y[:,0])

        self.x2 = self.L2i*sin(y[:,2]) + self.x1
        self.y2 = -self.L2i*cos(y[:,2]) + self.y1

        return self.x1,self.y1,self.x2,self.y2,y[:,0],y[:,2],y[:,1],y[:,3]

    def double(self):

    	self.fig.clear()

    	self.x1,self.y1,self.x2,self.y2,theta1,theta2,theta1dot,theta2dot=self.double_calc()

        

        canvas = FigureCanvasTkAgg(self.fig, master = self)
        canvas.get_tk_widget().grid(column=0,row=12,columnspan=10)

        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-5.0, 5.0), ylim=(-5.0, 5.0))
        ax.grid()

        self.line, = ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        self.time_text.set_text('')

        self.ani = animation.FuncAnimation(self.fig, self.doubleanimate, np.arange(1, len(theta1)),
            interval=25, blit=False, init_func=self.init,repeat=False)

        #ani.save('double_pendulum.mp4', fps=15)
        canvas.show()

    def doubleanimate(self, i):
        self.thisx = [0, self.x1[i], self.x2[i]]
        self.thisy = [0, self.y1[i], self.y2[i]]

        self.line.set_data(self.thisx, self.thisy)
        self.time_text.set_text(self.time_template%(i*self.dt))
        return self.line, self.time_text
    def rad2deg(self,vector):
    	deg_vect=[rad*180/np.pi for rad in vector]
    	return deg_vect

    def plot(self):

    	if 'key' in locals():
    		del key

    	if 'parameter2'in locals():
    		del parameter2



    	self.sx1,self.sy1,self.stheta,self.sthetadot=self.single_calc()

        self.dx1,self.dy1,self.dx2,self.dy2,self.dtheta1,self.dtheta2,self.dtheta1dot,self.dtheta2dot=self.double_calc()

        self.stheta=self.rad2deg(self.stheta)
        self.sthetadot=self.rad2deg(self.sthetadot)
        self.dtheta1=self.rad2deg(self.dtheta1)
        self.dtheta1dot=self.rad2deg(self.dtheta1dot)
        self.dtheta2=self.rad2deg(self.dtheta2)
        self.dtheta2dot=self.rad2deg(self.dtheta2dot)

    	string=self.var.get()
    	

    	if string=='Single X-Position':
    		parameter=self.sx1
    		label='X-Position'
    	elif string=='Single Y-Position':
    		parameter=self.sy1
    		label='Y-Position'
    	elif string=='Single Angle':
    		parameter=self.stheta
    		label='Theta (deg)'
    	elif string=='Single Angular Velocity':
    		parameter=self.sthetadot
    		label='Omega (deg/s)'

    	elif string=='Double X-Position':
    		parameter=self.dx1
    		parameter2=self.dx2
    		label='X-Position'
    	elif string=='Double Y-Position':
    		parameter=self.dy1
    		parameter2=self.dy2
    		label='Y-Position'
    	elif string=='Double Angle':
    		parameter=self.dtheta1
    		parameter2=self.dtheta2
    		label='Theta'
    	elif string=='Double Angular Velocity':
    		parameter=self.dtheta1dot
    		parameter2=self.dtheta2dot
    		label='Omega'

    	if string=='Single X-Position' or string=='Single Y-Position' or string=='Single Angle' or string=='Single Angular Velocity':
    		title='Single Pendulum'
    	else:
    		title='Double Pendulum'
    		key=['Link1', 'Link2']
    
    	self.fig2.clear()
    	canvas2 = FigureCanvasTkAgg(self.fig2, master = self)
        canvas2.get_tk_widget().grid(column=11,row=12,columnspan=10)
        



        
        

    	if 'parameter2'in locals():
    		min1=min(parameter)
    		min2=min(parameter2)
    		min_real=min(min1,min2)

    		max1=max(parameter)
    		max2=max(parameter2)
    		max_real=max(max1,max2)

    		ax2 = self.fig2.add_subplot(111, autoscale_on=False, xlim=(min(self.t), max(self.t)), ylim=(min_real,max_real))
    		ax2.grid()
        	ax2.plot(self.t,parameter,self.t,parameter2)
        else:
        	ax2 = self.fig2.add_subplot(111, autoscale_on=False, xlim=(min(self.t), max(self.t)), ylim=(min(parameter), max(parameter)))
    		ax2.grid()
        	ax2.plot(self.t,parameter)

        plt.ylabel(label)
        plt.xlabel('Time (s)')
       	plt.title(title)

       	if 'key' in locals():
       		plt.legend(key)

        canvas2.show()

    

        

def main():
#used double pendulum code obtained from matplotlib 'http://matplotlib.org/examples/animation/double_pendulum_animated.html'
    root = tk.Tk()

    gui=Interface(master=root)
    gui.master.title("Pendulum Simulation")
    gui.mainloop()


main()