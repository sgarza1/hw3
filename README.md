```
#!python


```
Stephen Garza
Monica Parucho
Pseudo Code (the format gets altered badly even though it looks correct in the text box. I also sent the code in an email to the TAs.)

Create and define class for GUI
	class Interface()
Create widgets for GUI
	For both links: angle, length, mass, friction, and angular velocity
	Create label, place on grid
	Create horizontal scale, place on grid
	Set default
	Create time and time interval entry
o	Use StringVar to create time variable
o	Create entry box, place on grid
o	Set time default
-	Create animate, quit, and plot buttons/menus
o	quit=Button(text, command = self.quit).grid(column,row)
Calculate Data and Output
-	Get parameters from widgets
o	L1i=length1.get()
o	M1i=mass1.get()
o	F1i=friction1.get()
o	Th1=angle1.get()
o	W1=angvel1.get()
-	Compute data for x, y, theta, and omega using widget information and dy/dx equations
o	function single_derivatives(state,t):
o	function double_derivatives(state,t):

-	Solve ODE equation and save results
o	function single_calc():
y=integrate(single_derivatives)

o	function double_calc():
Y=integrate(double_derivatives)

-	Detect request for action (button clicks for either animate, quit, or plot)
o	If button pressed is “animate single”: #command = single
•	function single():
Clear figure
Get data from single_calc()
Plot animation

o	If button pressed is “animate double”: #command=double
o	If button pressed is “plot”: #command = plot
•	function plot():

Get string variable from drop-down options menu

For each string variable:
	If string_variable = option_1
		Parameter = data_1
		Parameter2 = data_2
		Label = “option_1_label”
…
If single plot:
plot(t, parameter)

If double plot:
plot(t,parameter,t,parameter2)

Quit
-	If Quit button pressed: Close GUI
o	self.quit